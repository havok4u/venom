# Welcome

Welcome to (**VENOM** **V**irtual **E**ngineering **N**etwork **O**perations **M**anager )

This application serves as a wrapper to kvm objects whether they be router, server or other
images that run under kvm.  Please use Documentation and Video's found in the Wiki section to
aide in learning how to use VENOM.
