#!/usr/bin/env python

################################################################################
# <copyright file="gnui.py" owner="Tim Epkes and Paul Duda">
# Copyright (c) 2013 All Right Reserved, http://bitbucket.com/generatenetwork
#
# Please see the LICENSE.txt file for more information.
# All other rights reserved.
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
#
# </copyright>
# <author>Tim Epkes and Paul Duda</author>
# <email>tim.epkes@gmail.com and paul@zippity.net</email>
# <date>2013-01-20</date>
# <summary>Contains code for menu system</summary>
################################################################################

################################################################################
# Todo:  
#     1.  Add the ability to tie taps to the management network
#     2.  Add the ability to autogen link IP's and show code snippets
#     3.  Edit a devices memory, Interface type and more (maybe)
#
################################################################################

import pickle
import re
import commands
from networkdevice import device
from os import path
import os
from parseospf import parseospf
from parseisis import parseisis
from parsegml import parsegml
import argparse

# this needs to be cleaned up, it is out of control
def main():
    def deviceModelOptions(type, mode):
        model_menu = {'title': 'Select a router model to add:', 'options': {} }
        item_no = 0
        for d in sorted(device.defaults.keys()):
            if device.defaults[d]['type'] ==  type:
                item_no += 1
                if mode == 'bulk':
                    option = { 'item': d, 'method': addBulkDevice, 'parameter': {'type': type, 'model': d}}
                else:
                    option = { 'item': d, 'method': addDevice, 'parameter': {'type': type, 'model': d} }
                model_menu['options'][str(item_no)] = option
            # Insert another if for a passed in param indicating bulk or single add
        model_menu['options']['b'] = {'item': 'Back to Device Operations', 'menu': device_operations_menu }
        model_menu['options']['r'] = {'item': 'Return to main menu'}
        return model_menu

    def addDevice(type, model):
        name=None
        if type == 'server':
            print "Add server is not implemented."
            return None
        if (device.kvm_defaults['name_flag']):
            name = raw_input("Enter Device name: ")
        ui = raw_input("How many interfaces? ") or '0'
        if not re.match('^\d+$', ui):
            ui = '0'
        ui = int(ui)   # need ability to put in router type
        if ui > 0:
            if not name:
                d = device(model=model,interfaces=ui)
            else:
                d = device(model=model,interfaces=ui,name=name)
        else:
            if not name:
                d = device(model=model)
            else:
                d = device(model=model,name=name)
        print "Created a %s with %d interfaces.\n" % ( type.lower(), ui)

    def addBulkDevice(type, model):
        name=None
        if type == 'server':
            print "Add bulk server is not implemented."
            return None
        dev_num = raw_input ("How many devices? : ") or '0'
        if not re.match('^\d+$', dev_num):
            dev_num = '0'
        dev_num = int(dev_num)
        ui = raw_input ("How many interfaces per device? : ") or '0'
        if not re.match('^\d+$', ui):
            ui = '0'
        ui = int(ui)
        for i in range (0,dev_num):
            d = device(model=model,interfaces=ui)
            print "Created a %s with %d interfaces.\n" % ( type.lower(), ui)

    def addInterfaces(d):
        ui = raw_input("How many interfaces? ")
        ui = int(ui)
        if ui > 0:
            for i in range(0, ui):
                d.addInterface()
    
    def addLink(start, end):
        # need to rework this method. Input is either an interface or a device
        if device.kvm_defaults['autoassign_int'] == 1:
            start.createLink(end)
        else:
            start.parent.createLink(end.parent, start_int=start.name, end_int=end.name)
        return device_operations_menu
    
    def linkOptions(d, start_i=None):
        """Display the available ports for the selected device. If the start device.port
           is already selected then the method call will be to create the link with the
           selected end device.port. """
        end_i = None
        if start_i:
            type = 'end'
        else:
            type = 'start'
        link_menu = {'title': 'Select a %s interface' % type, 'options': {} }
        item_no = 0
        for n, i in enumerate(d.interfaces):
            if not i.connection:
                item_no += 1
                if start_i:
                    option = { 'item': i.name, 'method': addLink,
                               'parameter': {'start': start_i, 'end': i} }
                else:
                    option = { 'item': i.name, 'method': interfaceOptions,
                               'parameter': {'mode': 'link', 'start_i': i} }
                link_menu['options'][str(item_no)] = option
        link_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': device_operations_menu }
        link_menu['options']['r'] = {'item': 'Return to main menu'}
        return link_menu

    def interfaceOptions(mode, start_i=None, start_d=None):
        """Generate a list of devices for the selection. This method is called with a start device
           or a start interface or none. Mode determines the method used in the menu."""
        if not len(device.devices) > 0:
            print "No devices to show."
            return
        if start_i:
            print 'got interface %s.%s' % ( start_i.parent.name, start_i.name )
        interface_menu = {'title': 'Select a device.', 'options': {}}
        for n, d in enumerate(device.devices):
            if mode == 'add':
                option = { 'item': d.name, 'method': addInterfaces, 'parameter': d }
            elif mode == 'show':
                option = { 'item': d.name, 'method': d.showInterfaces }
            elif mode == 'link':
                if (device.kvm_defaults['autoassign_int'] == 1):
                    # if start_d is defined then we are selecting the end device
                    if start_d:
                        option = { 'item': d.name, 'method': addLink,
                            'parameter': { 'start': start_d, 'end': d } }
                    else:
                        option = { 'item': d.name, 'method': interfaceOptions,
                                   'parameter': {'mode': 'link', 'start_d': d} }
                else:
                    option = { 'item': d.name, 'method': linkOptions, 'parameter': {'d': d, 'start_i': start_i} }
            interface_menu['options'][str(n + 1)] = option
        if mode == 'show':
            interface_menu['options']['a'] = {'item': 'All', 'method': device.showAllInterfaces}
        interface_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': device_operations_menu }
        interface_menu['options']['r'] = {'item': 'Return to main menu'}
        return interface_menu

    def updateMem(d):
        ui = raw_input("Enter a new memory value (512 - 8192): ")
        if ui:
            if (int(ui) > 8192 or int(ui) < 512):
                print "Invalid memory assigned"
            else:
                d.memory = ui 

    def updateAllMem():
        ui = raw_input("Enter a new memory value (512 - 8192): ")
        if ui:
            if (int(ui) > 8192 or int(ui) < 512):
                print "Invalid memory assigned"
            else:
                for n, d in enumerate(device.devices):
                    d.memory = ui 
           
    def changeMem():
        """Change device memory for one or all devices."""
        if not len(device.devices) >0:
            print "No devices to show."
            return
        device_menu = {'title': 'Select a device.', 'options': {}}
        for n, d in enumerate(device.devices):
            option = {'item': d.name, 'method': updateMem, 'parameter': d }
            device_menu['options'][str(n+1)] = option
        device_menu['options']['a'] = {'item': 'All', 'method': updateAllMem }
        device_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': config_operations_menu }
        device_menu['options']['r'] = {'item': 'Return to main menu'}
        return device_menu

    def printDeviceConf():
        if not len(device.devices) > 0:
            print "No devices to show.\n"
            return
        printdevice_menu = {'title': 'Select a device.', 'options': {}}
        for n, d in enumerate(device.devices):
            option = { 'item': d.name, 'method': d.showDeviceConf }
            printdevice_menu['options'][str(n + 1)] = option
        printdevice_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': config_operations_menu }
        printdevice_menu['options']['r'] = {'item': 'Return to main menu'}
        return printdevice_menu

    ### New routine
    def device_config_menu():
        if not len(device.devices) > 0:
            print "No devices to show.\n"
            return
        tog = { 0: 'Disabled', 1: 'Enabled' }
        dcd = device.devcfg_defaults
        cfgoptions_menu = {'title': 'Select a config option.', 'options': {}}

        cfgoptions_menu['options']['1'] = {'item': lambda : 'OSPF (%s)' % (tog[dcd['ospf_enable']] ),
            'method': toggle, 'parameter': 'ospf' }
        cfgoptions_menu['options']['2'] = {'item': lambda : 'ISIS (%s)' % (tog[dcd['isis_enable']] ),
            'method': toggle, 'parameter': 'isis' }
        cfgoptions_menu['options']['3'] = {'item': lambda : 'MPLS (%s)' % (tog[dcd['mpls_enable']] ),
            'method': toggle, 'parameter': 'mpls' }
        cfgoptions_menu['options']['4'] = {'item': lambda : 'MPLS TE (%s)' % (tog[dcd['mpls_te_enable']] ),
            'method': toggle, 'parameter': 'mpls_te' }

        cfgoptions_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': config_operations_menu }
        cfgoptions_menu['options']['r'] = {'item': 'Return to main menu'}
        return cfgoptions_menu

    def create_device_conf_iso():
        print "Creating ISOs"
        f = open('iosxr_config_admin.txt', 'w')
        f.write ('username cisco1\n')
        f.write (' group root-system\n')
        f.write (' secret 5 $1$mJgx$a.7sM3z/Qx18HCzVbPAym0\n')
        f.write ('!\n')
        f.write ('end\n')
        f.close()
        if not len(device.devices) >0:
            print "No devices to show.\n"
            return
        for d in device.devices:
            filename = d.name + '.iso'
            config = d.showDeviceConf(1)
            f = open ("iosxr_config.txt", 'w')
            for i in config:
                f.write(i + '\n') 
            f.close()
            result = commands.getstatusoutput('mkisofs -o %s -l --iso-level 2 iosxr_config.txt iosxr_config_admin.txt' % filename )
            #print result[1]
        result = commands.getstatusoutput('rm iosxr_config*')

    def printKVMConf():
        for d in device.devices:
            d.showKVMConf()

    def start_network():
        # Check to see if the network is already running
        if len(device.devices) > 0:
            if (path.isfile('start') == 1):
                result = commands.getstatusoutput('./start') 
                if (result[0] == 0):
                    print "Network successfully launched\n"
                else:
                    print "ALERT: Network launch was Un-successful"
                    print "result was %d and reason was %s\n" % (result[0], result[1])
                    stop_network() 
            else:
                print "A Network Build has not been created, please build the network and retry\n"

    def stop_network(): 
        if (path.isfile('stop')):
            commands.getstatusoutput('./stop')
            print "Network stopped\n"
        else:
            print "A Network Build has not been created, please build the network and retry\n"

    def build_startup():
        if (path.isfile('start')):
            print "Overwritting current start file\n"
        else: 
            print "Creating new start file\n"
        f = open("start",'w')
        f.write('#!/bin/sh\n')
        f.write('\n')
        for d in device.devices:
            d.showKVMConf(f)
        #f.write('\nreset\n') # causes an error when launched from here
        f.close()
        os.chmod('start',0775)

    def build_stop():
        if (path.isfile('stop')):
            print "Overwritting current stop file\n"
        else: 
            print "Creating new stop file\n"
        f = open("stop",'w')
        f.write('#!/bin/sh\n')
        f.write('\n')
        for d in device.devices:
            f.write ('kill `ps auwx | grep kvm | grep %s.img | grep %d | awk \'{print $2}\'`\n' % (d.name,d.telnet_port))
        f.close()
        os.chmod('stop',0775)

    def toggle(protocol):
        tog = { 0: 'Disabled', 1: 'Enabled' }
        option = protocol+'_enable'
        if ( device.devcfg_defaults[option] == 0):
            device.devcfg_defaults[option] = 1
            if (protocol == 'ospf'):
                if (device.devcfg_defaults['isis_enable'] == 1):
                    device.devcfg_defaults['isis_enable'] = 0
                    print "%s: %s" % (tog[device.devcfg_defaults['isis_enable']], 'isis')
            elif (protocol == 'isis'):
                if (device.devcfg_defaults['ospf_enable'] == 1):
                    device.devcfg_defaults['ospf_enable'] = 0
                    print "%s: %s" % (tog[device.devcfg_defaults['ospf_enable']], 'ospf')
        else:
            device.devcfg_defaults[option] = 0

        print "%s: %s\n" % (tog[device.devcfg_defaults[option]], protocol)
 
    def save_network(): 
        fileName = 'network.pkl'
        file = open(fileName, 'wb')
        config = {'kvm_defaults': device.kvm_defaults, 'device': device.devices, 'devcfg_defaults': device.devcfg_defaults}
        pickle.dump(config, file, pickle.HIGHEST_PROTOCOL)
        file.close()
        print "Setup Saved\n"

    def load_network(): 
        if (path.isfile('network.pkl')):
            fileName = 'network.pkl'
            file = open(fileName, 'rb')
            config = pickle.load(file)
            device.devices = config['device']
            device.kvm_defaults = config['kvm_defaults']
            device.devcfg_defaults = config['devcfg_defaults']
            file.close()
        else:
            print 'network.pkl file does not exist, please save setup after you build\n'

    def set_telnet():
        ui = raw_input("Set default telnet/terminal server listening port [current: %d] [range: 2000 - 65000 ]: " % device.kvm_defaults['start_telnet'])
        if (ui):
            ui = int(ui)
            if (ui > 1999)  and (ui < 65000):
                device.kvm_defaults['start_telnet'] = ui
            else:
                print "ALERT: Port entered is not in specified range\n" 
        else:
            print "No value entered\n"

    def set_tcp():
        ui = raw_input("set default TCP port [current: %d] [range: 2000 - 65000 ]: " % device.kvm_defaults['start_tcp'])
        if (ui):
            ui = int(ui)
            if (ui > 1999)  and (ui < 65000):
                device.kvm_defaults['start_tcp'] = ui
            else:
                print "ALERT: Port entered is not in specified range\n"
        else:
            print "No value entered\n"

    def toggle_name_flag():
        if ( device.kvm_defaults['name_flag'] == 0):
            device.kvm_defaults['name_flag'] = 1
            print "Enabled\n"
        else:
            device.kvm_defaults['name_flag'] = 0
            print "Disabled\n"

    def set_image_path():
        ui = raw_input("Set Image Path [%s]:  " % device.kvm_defaults['path'])
        if (ui):
            if (path.isdir(ui)):
                device.kvm_defaults['path'] = ui
                print "Image path is %s\n" % ui
            else:
                print "\nALERT: Directory does not exist\n"
        else:
            print "Default path %s preserved\n" % device.kvm_defaults['path']

    def set_image(image):
        ui = raw_input("Set %s path, current[%s]: " % (image,device.defaults[image]['image']))
        if (ui):
            imagepath = device.kvm_defaults['path']+'/'+ui
            if (path.isfile(imagepath)):
                device.defaults[image]['image'] = ui
                print "%s image is now set to %s" % (image,ui)
            else:
                print "Image %s does not exist in %s." % (ui,device.kvm_defaults['path'])
        return

    def build_network():
        if (path.isdir(device.kvm_defaults['path'])):
            if len(device.devices) > 0:
                flag = 0
                for i in device.devices:
                    filename = i.name + '.img'
                    if not (path.isfile(filename)):
                        result = commands.getstatusoutput('qemu-img create -b %s/%s -f qcow2 %s.img' % 
                                (device.kvm_defaults['path'],device.defaults[i.model]['image'],i.name))
                        if (result[0] >0):
                            print "ALERT: qemu-img create failed from base image %s/%s destination %s.img, please check our permissions or path" % (device.kvm_defaults['path'], device.defaults[i.model]['image'],i.name)
                            flag = 1
                        else: 
                            print "Created %s\n" % filename
                    else:
                        print "Device %s already exists\n" % i.name
                if (flag == 0):
                    build_startup()
                    build_stop()
                    create_device_conf_iso()
                else:
                    print "Due to image creation failure, start and stop scripts not built\n"
            else:
                print "ALERT: No devices built"
        else:
            print "ALERT: Image path is %s, Please change the image path!\n" % device.kvm_defaults['path']

    def view_running_devices():
        if len(device.devices) > 0:
            print '%-15s %-10s %-15s %-15s' % ("Device", "State", "PID", "Console")
            print '%-15s %-10s %-15s %-15s' % ('-'*15, '-'*10, '-'*15, '-'*15)
            for d in device.devices:
                pid = commands.getoutput('ps auwx | grep kvm | grep %s.img | grep %d | egrep -v sh | awk \'{print $2}\''  % (d.name,d.telnet_port))
                if (pid): 
                    if (device.defaults[d.model]['vnc'] == 0):
                        print  '%-15s %-10s %-15s telnet: %-15s' % (d.name,"Running",pid,d.telnet_port)
                    elif (device.defalts[d.model]['vnc'] == 1):
                        print '%-15s %-10s %-15s telnet: %-15s' % (d.name,"Running",pid,d.vnc_port)
            print ("\n\n")

    def delInterface():
        """Delete an interface and its associated Links"""
        pass

    def delLink(l):
        """Delete links"""
        l.delete()
        return link_options()

    def delDevice(d):
        """Delete a device and all of its associated links"""
        d.delete()
        return device_menu('del')

    def renDevice(d):
        """Change the name of a device"""
        ui = raw_input("Enter the new name of device or enter to keep: ")
        if ui:
            d.name = ui
        return device_menu('change')

    def device_menu(mode):
        """Overloaded Device menu for adds or deletes"""
        if mode == 'add' or mode == 'bulk':
                title = 'Select a config option.'
        elif mode == 'del':
                title = 'Select a device to delete.'
        elif mode == 'change':
                title = 'Select a device to rename.'
        device_menu= {'title': title, 'options': {}}
        if mode == 'add' or mode == 'bulk':
            device_menu['options']['1'] = {'item': 'Router', 'method': deviceModelOptions, 'parameter': {'type': 'router', 'mode': mode }}
            device_menu['options']['2'] = {'item': 'Server', 'method': deviceModelOptions, 'parameter': {'type': 'server', 'mode': mode }}
        elif mode == 'del' or mode == 'change':
            if len(device.devices) > 0:
                for n,d in enumerate(device.devices):
                    if mode == 'del':
                        device_menu['options'][str(n+1)] = {'item': d.name, 'method': delDevice, 'parameter': d }
                    if mode == 'change':
                        device_menu['options'][str(n+1)] = {'item': d.name, 'method': renDevice, 'parameter': d }
            else: 
                print "No devices available to delete.\n"
                return device_operations_menu 
        device_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': device_operations_menu }
        device_menu['options']['r'] = {'item': 'Return to main menu'}
        return device_menu

    def link_options():
        link_menu= {'title': 'Select a link to delete', 'options': {}}
        for n, l in enumerate(device.getAllLinks()):
            link_name = '%s.%s to %s.%s' % ( l.start_device.name, l.start_int.name,
                                             l.end_device.name, l.end_int.name)
            link_menu['options'][str(n+1)] = {'item': link_name, 'method': delLink, 'parameter': l}
        link_menu['options']['b'] = {'item': 'Back to Device menu', 'menu': device_operations_menu }
        link_menu['options']['r'] = {'item': 'Return to main menu'}
        return link_menu

    def menuInt(value):
       try:
           result = int(value)
       except ValueError:
           result = value
       return result

    menu = True

    image_options_menu = {
        'title': 'Select an option.',
        'options': {'1': {'item': 'Set Image PATH', 'method': set_image_path },
                    '2': {'item': 'Set default XRVR Image', 'method': set_image, 'parameter': 'XRVR'}, 
                    '3': {'item': 'Set default vIOS Image', 'method': set_image, 'parameter': 'vIOS'},  
                    '4': {'item': 'Set default N7K Image', 'method': set_image, 'parameter': 'N7K'},  
                    'r': {'item': 'Return to main menu'} }
    }

    settings_menu = {
        'title': 'Select an option.',
        'options': {'1': {'item': 'Image Options', 'menu': image_options_menu },
                    '2': {'item': 'Set telnet start port', 'method': set_telnet },
                    '3': {'item': 'Set TCP start port', 'method': set_tcp },
                    '4': {'item': 'Toggle override device name flag', 'method': toggle_name_flag },  
                    'r': {'item': 'Return to main menu'} }
    }

    device_operations_menu = {
        'title': 'Select an action.',
        'options': {'1': {'item': 'Add Device', 'method': device_menu, 'parameter': 'add'},
                    '2': {'item': 'Delete Device', 'method': device_menu, 'parameter': 'del'},
                    '3': {'item': 'Show Device', 'method': device.showDevices },
                    '4': {'item': 'Bulk Device Add','method': device_menu, 'parameter': 'bulk'},
                    '5': {'item': 'Add Interface', 'method': interfaceOptions, 'parameter': 'add'},
                    '6': {'item': 'Show Interface', 'method': interfaceOptions, 'parameter': 'show'},
                    '7': {'item': 'Add Link', 'method': interfaceOptions, 'parameter': 'link'},
                    '8': {'item': 'Delete Link' },   ### Need to code delete Link
                    '9': {'item': 'Show Link', 'method': device.showAllLinks },
                    '10': {'item': 'Change Device Name', 'method': device_menu, 'parameter': 'change'},
                    'r': {'item': 'Return to main menu'} }
    }

    config_operations_menu = {
        'title': 'Select an action.',
        'options': {'1': {'item': 'Print KVM Config', 'method': printKVMConf },
                    '2': {'item': 'Print Device Config', 'method': printDeviceConf },
                    '3': {'item': 'Protocol Options', 'method': device_config_menu },
                    '4': {'item': 'Change Device Memory','method': changeMem },
                    'r': {'item': 'Return to main menu'} }
    }

    network_operations_menu = {
        'title': 'Select an action.',
        'options': {'1': {'item': 'Start Network', 'method': start_network },   
                    '2': {'item': 'Stop  Network', 'method': stop_network },
                    '3': {'item': 'View Running Devices', 'method': view_running_devices },
                    '4': {'item': 'Build Network', 'method': build_network }, 
                    'r': {'item': 'Return to main menu'} }
    }

    main_menu = {
        'title': 'Select an action.',
        'options': {'1': {'item': 'Device Operations', 'menu': device_operations_menu },
                    '2': {'item': 'Network Operations', 'menu': network_operations_menu },
                    '3': {'item': 'Config Operations', 'menu': config_operations_menu },
                    '4': {'item': 'Settings', 'menu': settings_menu },
                    '5': {'item': 'Save Network Setup', 'method': save_network},
                    '6': {'item': 'Export GML', 'method': device.exportGML},
                    'q': {'item': 'Quit' } }
    }

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--importospf", help="Import an OSPF file")
    parser.add_argument("-j", "--importisis", help="Import an ISIS file")
    parser.add_argument("-g", "--importgml", help="Import a GML file")
    args = parser.parse_args()

    # TODO: This is not DRY
    if args.importospf:
        if path.isfile(args.importospf):
            file = open(args.importospf, 'r')
            input = file.readlines()
            ospf = parseospf(input)
            if ospf:
                print 'Info: Loading ospf file %s.' % args.importospf
                ospf.createRouters()
                ospf.createLinks()
            file.close()
        else:
            print "Error: can't load ospf file %s." % args.importospf
    elif args.importisis:
        if path.isfile(args.importisis):
            file = open(args.importisis, 'r')
            input = file.readlines()
            isis = parseisis(input)
            if isis:
                print 'Info: Loading isis file %s.' % args.importisis
                isis.createRouters()
                isis.createLinks()
            file.close()
        else:
            print "Error: can't load isis file %s." % args.importisis
    elif args.importgml:
        if path.isfile(args.importgml):
            file = open(args.importgml, 'r')
            input = file.readlines()
            gml = parsegml(input)
            if gml:
                print 'Info: Loaded gml file %s.' % args.importgml
            file.close()
        else:
            print "Error: can't load gml file %s." % args.importgml
    elif (path.isfile('network.pkl')):
        fileName = 'network.pkl'
        file = open(fileName, 'rb')
        config = pickle.load(file)
        device.devices = config['device']
        device.kvm_defaults = config['kvm_defaults']
        device.devcfg_defaults = config['devcfg_defaults']
        file.close()


    current_menu = main_menu

    while menu:
        print current_menu['title']
        for k in sorted(current_menu['options'].keys(), key=menuInt):
            if type(current_menu['options'][k]['item']) == type(lambda x: x):
                print '    %s. %s' % ( k, current_menu['options'][k]['item']() )
            else:
                print '    %s. %s' % ( k, current_menu['options'][k]['item'] )
        ui = raw_input("Action? ").lower()
        result = None
        if ui in current_menu['options'].keys():
            if ui == 'q':
                menu = False
                print 'Bye, bye.'
            elif ui == 'r':
                current_menu = main_menu
            else:
                print 'you picked %s\n' % current_menu['options'][ui]['item']
                if current_menu['options'][ui].has_key('method'):
                    if current_menu['options'][ui].has_key('parameter'):
                        parameter = current_menu['options'][ui]['parameter']
                        if type(parameter) == type(dict()):
                            result = current_menu['options'][ui]['method'](**parameter)
                        else:
                            result = current_menu['options'][ui]['method'](parameter)
                    else:
                        result = current_menu['options'][ui]['method']()
                if result:
                    current_menu = result
                elif current_menu['options'][ui].has_key('menu'):
                    current_menu = current_menu['options'][ui]['menu']
        else:
            print 'Bad input: %s' % ui

if __name__ == '__main__':
    main()

