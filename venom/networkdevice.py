#!/usr/bin/env python

################################################################################
# <copyright file="NetworkDevice.py" owner="Tim Epkes and Paul Duda">
# Copyright (c) 2013 All Right Reserved, http://bitbucket.com/generatenetwork
#
# Please see the LICENSE.txt file for more information.
# All other rights reserved.
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
#
# </copyright>
# <author>Tim Epkes and Paul Duda</author>
# <email>tim.epkes@gmail.com and paul@zippity.net</email>
# <date>2013-01-20</date>
# <summary>Contains a base, abstract class for a device</summary>
################################################################################

 
import sys

# device class
class device(object):
    """ Common base class for device """
    kvm_defaults = {
        'device_num': 0, 'start_telnet': 7000, 'start_tcp': 9000, 'start_vlan': 200,
        'vnc_port': 1, 'mac': '00:01:00:00:00:00', 'path': '/opt/images', 'name_flag': 0,
        'autoassign_int': 1, 'tapassignment':1,
    }
    devcfg_defaults = {
        'ip_link': '192.168.0.0', 'ip_loop': '10.1.0.1', 'ospf_enable': 0,
        'isis_enable': 0, 'mpls_enable': 0, 'mpls_te_enable': 0, 'isis_net': 1
    }
    devices = []            # keep track of devices that are created
    mgmt_vlan = 1000        # the first interface on each device and unused interfaces use this vlan
    defaults = {
        'XRVR': { 'memory': 2048, 'boot': 'hda', 'int_model': 'virtio', 'int_prefix': 'g0/0/0/',
                  'name_prefix': 'R', 'bits': 32, 'vnc': 0, 'type': 'router', 'mgmt': 'MgmtEth0/0/CPU0/0',
                  'image': 'xrvr-4.3.2' },
        'N7K': { 'memory': 1536, 'boot': 'hda', 'int_model': 'e1000', 'int_prefix': 'e2/',
                  'name_prefix': 'SR', 'bits': 64, 'vnc': 0, 'type': 'router', 'image': 'n7k-5.2-1.N1.1'},
        'vIOS': { 'memory': 1024, 'boot': 'cdrom', 'int_model': 'e1000', 'int_prefix': 'g0/',
                 'name_prefix': 'R', 'bits': 64, 'vnc': 0, 'type': 'router',
                 'image': 'vios-adventerprisek9-m.iso' },
        'Ultra': {'memory': 2048, 'boot': 'cdrom', 'int_model': 'e1000', 'int_prefix': 'g0/',
                 'name_prefix': 'R', 'bits': 64, 'vnc': 0, 'type': 'router', },
        'WWW': { 'memory': 1024, 'boot': 'hda', 'int_model': 'e1000', 'int_prefix': 'eth',
                 'name_prefix': 'WWW', 'bits': 64, 'vnc': 1, 'type': 'server', },
        'DB': { 'memory': 1024, 'boot': 'hda', 'int_model': 'e1000', 'int_prefix': 'eth',
                 'name_prefix': 'DB', 'bits': 64, 'vnc': 1, 'type': 'server', },
        'Ostinato': { 'memory': 2048, 'boot': 'hda', 'int_model': 'e1000', 'int_prefix': 'eth',
                 'name_prefix': 'DB', 'bits': 64, 'vnc': 1, 'type': 'tg', },
        'Drone': { 'memory': 2048, 'boot': 'hda', 'int_model': 'e1000', 'int_prefix': 'eth',
                 'name_prefix': 'DB', 'bits': 64, 'vnc': 1, 'type': 'drone', },
    }
 
    def __init__ (self, model=None, name=None, telnet_port=None, int_prefix=None, name_prefix=None, bits=0,
                  vnc=None, memory=None, boot=None, int_model=None, interfaces=0, start_vlan=None,
                  ip_loop=None, vnc_port=None):
        device.devices.append(self)
        self.telnet_port = telnet_port or device.kvm_defaults['start_telnet']
        device.kvm_defaults['start_telnet'] += 1
        self.vnc_port = vnc_port or device.kvm_defaults['vnc_port']
        device.kvm_defaults['vnc_port'] += 1
        self.model = model or 'XRVR'
        if not device.defaults.has_key(self.model):
            print '%s is not a valid device model, exiting...' % self.model
            sys.exit(1)
        self.type = device.defaults[self.model]['type']
        self.name = name or device.defaults[self.model]['name_prefix'] + str(device.kvm_defaults['device_num'])
        self.int_prefix = int_prefix or device.defaults[self.model]['int_prefix'] 
        self.bits= bits or device.defaults[self.model]['bits'] 
        self.vnc= vnc or device.defaults[self.model]['vnc'] 
        self.memory = memory or device.defaults[self.model]['memory']
        self.boot = boot or device.defaults[self.model]['boot']
        self.int_model = int_model or device.defaults[self.model]['int_model']
        # probably should check that interfaces is an integer
        self.start_vlan = start_vlan or device.kvm_defaults['start_vlan']
        self.ip_loop = ip_loop or device.devcfg_defaults['ip_loop']  ### New Loop Assignment
        device.devcfg_defaults['ip_loop'] = self.allocateLoopIP(1)
        self.interfaces = []
        self.device_num = len(device.devices)
        self.options = {}
 
        # let's create some interfaces if the user specified
        # an interface count
        for i in range(0, interfaces):
            self.addInterface()
        device.kvm_defaults['device_num'] += 1
 
    def __del__(self):
        # what to do when a device is deleted.
        # like remove related interface and connection records
        #for i in self.interfaces:
        #    if i.connection:
        #        i.connection.end_int.connection = None
        #        i.connection = None
        pass
 
    def __str__(self):
        return "%-20s %-10s %-11s %-10s %-10d" % (  
            self.name, self.model, self.telnet_port, self.memory,len(self.interfaces) )
 
    def delete(self):
        for i in self.interfaces:
            if i.connection:
                print 'deleting', i.connection
                i.connection.delete()
        device.devices.remove(self)

    #@classmethod
    #def reset(self):
    #    self.kvm_defaults = {'device_num': 0 ,'start_telnet': 7000, 'start_tcp': 9000, 'start_vlan': 200,
    #                'vnc_port': 1, 'mac': '00:01:00:00:00:00', 'path': '/opt/images', 'name_flag': 0  }
    #    self.devcfg_defaults = { 'ip_link': '192.168.0.0', 'ip_loop': '10.1.0.1', 'ospf_enable': 0,
    #                    'isis_enable': 0, 'mpls_enable': 0, 'mpls_te_enable': 0 }
    #    self.devices = []            # keep track of devices that are created

    @classmethod
    def getDeviceStr(self):
        return "device last_device=%d, memory=%d last_telnet_port=%d, name_prefix=%s" % (
            self.device_num, self.memory, self.start_telnet, self.name_prefix)
 
    @classmethod
    def getDevice(self, name):
        """Get a device by name."""
        for d in self.devices:
            if d.name == name:
                return d
        return None

    def displayDeviceCount (self):
        print "Device count %d" % len(device.devices)
 
    def displayInterfaceCount(self):
        print "%s interface Count %d" % ( self.name, len(self.interfaces) )
 
    def addInterface(self, vlan=0, mac=None):
        if device.defaults[self.model].has_key('mgmt') and len(self.interfaces) == 0:
            inum = 0
            name = device.defaults[self.model]['mgmt']
            vlan = vlan or self.mgmt_vlan
        elif device.defaults[self.model].has_key('mgmt'):
            inum = len(self.interfaces) - 1
            name = self.int_prefix + str(inum)
        else:
            inum = len(self.interfaces)
            name = self.int_prefix + str(inum)
            if inum == 0:
                vlan = vlan or self.mgmt_vlan
        mac = device.allocateMac()
        model = self.int_model
       
        self.interfaces.append(interface(self, name, vlan, model, mac))
 
    @classmethod
    def allocateMac(self):
        mac = self.kvm_defaults['mac']
        maci = int(mac.replace(':', ''), 16) + 1
        mac = ':'.join(s.encode('hex') for s in ('%012x' % maci).decode('hex'))
        self.kvm_defaults['mac'] = mac
        return mac

    def getInterface(self, name):
        """Return an interface by name or index."""
        if type(name) == type(int()):
            if name < len(self.interfaces):
                return self.interfaces[name]
        else:
            for i in self.interfaces:
                if i.name == name: return i
        return None

    def createLink(self, end_device, vlan=None, start_int=None, end_int=None):
        # find unused interfaces on start and end devices
        start_interface = None
        end_interface = None
        if not vlan:
            vlan = device.kvm_defaults['start_vlan']
            device.kvm_defaults['start_vlan'] += 1

        if start_int:
            i = self.getInterface(start_int)
            if i and not i.connection:
                start_interface = i
            else:
                print 'Error: start interface %s does not exist or is in use.' % start_int
                return None
        else:
            # find the next unused interface on the start router
            for n, i in enumerate(self.interfaces):
                if n == 0: continue # first interface is for management
                if not i.connection and not start_interface:
                    start_interface = i

        if end_int:
            i = end_device.getInterface(end_int)
            if i and not i.connection:
                end_interface = i
            else:
                print 'Error: end interface %s does not exist or is in use.' % end_int
                return None
        else:
            # find the next unused interface on the end router
            for n, i in enumerate(end_device.interfaces):
                if n == 0: continue # first interface is for management
                if not i.connection and not end_interface:
                    end_interface = i
        if not start_interface or not end_interface:
            print "Error: Not enough unused interfaces to create connection."
            return None

        linkips = self.allocateLinkIP(2)
        start_interface.ip = linkips[0] 
        end_interface.ip = linkips[1] 
        start_interface.type = 'link'
        end_interface.type = 'link'
        start_interface.vlan = vlan
        end_interface.vlan = vlan
        this_link = link( self, end_device, vlan, device.kvm_defaults['start_tcp'],
            start_interface, end_interface)
        device.kvm_defaults['start_tcp'] += 1
        start_interface.connection = this_link
        end_interface.connection = this_link
        return this_link

    def allocateLoopIP(self, ip_incr=1):
        ip = self.ip_loop
        octet1=int((ip.split('.',3))[0])
        octet2=int((ip.split('.',3))[1])
        octet3=int((ip.split('.',3))[2])
        octet4=int((ip.split('.',3))[3])
        if (octet4 != 255):
            octet4 += ip_incr
        else:
            if (octet3 != 255):
                octet3+= ip_incr 
                octet4 = ip_incr
            else:
                octet3 = 0
                octet4 = 0
                print "All out of IP's addresses"
        ip = str(octet1) + '.' + str(octet2) + '.' + str(octet3) + '.' + str(octet4)
        return ip

    def allocateLinkIP (self, ip_incr=2):
        ips = []
        ip = device.devcfg_defaults['ip_link']
        octet1=int((ip.split('.',3))[0])
        octet2=int((ip.split('.',3))[1])
        octet3=int((ip.split('.',3))[2])
        octet4=int((ip.split('.',3))[3])
        if (octet4 != 255):
            octet4 += ip_incr
        else:
            if (octet3 != 255):
                octet3+= ip_incr
                octet4 = ip_incr
            else:
                octet3 = 0
                octet4 = 0
                print "All out of IP's addresses"
        ip = str(octet1) + '.' + str(octet2) + '.' + str(octet3) + '.' + str(octet4) 
        ips.append(ip)
        device.devcfg_defaults['ip_link'] = ip  ## ensure you set defaults to the first IP
        ip = str(octet1) + '.' + str(octet2) + '.' +  str(octet3) + '.' + str(octet4+1) 
        ips.append(ip)
        return ips
 
    @classmethod
    def showDevices(self, type=None):
        print "%-20s %-10s %-11s %-10s %-10s" % ( 'Name', 'Model', 'Telnet', 'Memory', 'Interfaces' )  
        print '%s %s %s %s %s' % ( '-' * 20, '-' * 10, '-' * 11, '-' * 10, '-' * 10 )
        for d in device.devices:
            if type:
                if d.type == type:
                    print d
            else:
                print d
        print "\n"

    def showInterfaces(self):
        print "%-30s %-10s %-6s %-17s %-43s" % ("Name","Model","VLAN","MAC", "Comment")
        print '%s %s %s %s %s' % ( '-' * 30, '-' * 10, '-' * 6, '-' * 17, '-' * 43 )
        for i in self.interfaces:
            print i
        print "\n"
 
    def showConnections(self, connections=None):
        # this will have to change to be tcp port or tap or other
        print "%-15s %-15s %-10s %-10s" % ("Source","Destination","VLAN","TCP Port")
        print '%s %s %s %s' % ( '-' * 15, '-' * 15, '-' * 10, '-' * 10)
        # TODO: maybe print no connections found if there are no connections
        if connections:
            for c in connections:
                print c
        else:
            for i in self.interfaces:
                if i.connection:
                    print i.connection
        print "\n"
    
 
    @classmethod
    def showAllLinks(self):
        # iterate over all interfaces on all devices and identify links
        connections = []
        for d in device.devices:
            for i in d.interfaces:
                if i.connection and connections.count(i.connection) == 0:
                    connections.append(i.connection)
        if connections:
            print "%-25s %-25s %-10s %-10s" % ("Source","Destination","VLAN","TCP Port")
            print '%s %s %s %s' % ( '-' * 25, '-' * 25, '-' * 10, '-' * 10)
            for c in connections:
                print c
            print "\n"

    @classmethod
    def getAllLinks(self):
        # iterate over all interfaces on all devices and identify links
        links = []
        for d in device.devices:
            for i in d.interfaces:
                if i.connection and links.count(i.connection) == 0:
                    links.append(i.connection)
        return links

    @classmethod
    def showAllInterfaces(self):
        for d in device.devices:
            d.showInterfaces()

    @classmethod
    def exportGML(self):
        # TODO: clean this up
        if not len(self.devices): return
        links = []
        f = open('network.gml', 'w')
        f.write('graph [\n    comment "%s"\n    directed %d\n    id %d\n    label "%s"\n' % (
            'gnui generated network', 1, 42, 'network' ) )
        device_names = []
        for n, d in enumerate(self.devices):
            # node [ id 0 label R0 ]
            device_names.append(d.name)
            f.write('    node [\n        id %d\n        label "%s"\n    ]\n' % (n, d.name) )
            for i in d.interfaces:
                if i.connection and links.count(i.connection) == 0:
                    links.append(i.connection)
        for l in links:
            # edge [ source 0 target 1 label "R0.g0/0/0/0 to R1.g0/0/0/0" ]
            start_dev = l.start_device
            start_int = l.start_int
            end_dev = l.end_device
            end_int = l.end_int
            source_id = device_names.index(start_dev.name)
            target_id = device_names.index(end_dev.name)
            link_name = '%s.%s to %s.%s' % (start_dev.name, start_int.name, end_dev.name, end_int.name)
            f.write('    edge [\n        source %d\n        target %d\n        label "%s"\n    ]\n' % (
                source_id, target_id, link_name) )
        f.write(']\n')
        f.close()
    
    def showDeviceConf(self, fp=None):
        # TODO: create a template for this
        config = []
        if (self.model == 'XRVR'):
            # Base Router configuration
            config.append ('hostname %s' % self.name)
            config.append ('!')
            config.append ('telnet vrf default ipv4 server max-servers 10')
            config.append ('line console')
            config.append (' timeout login response 0')
            config.append ('!')
            config.append ('control-plane')
            config.append (' management-plane')
            config.append ('  out-of-band')
            config.append ('   interface MgmtEth0/0/CPU0/0')
            config.append ('    allow all')
            config.append ('   exit')
            config.append ('  exit')
            config.append ('!')
            config.append ('interface Loopback 0')
            config.append (' ipv4 address %s/32' % (self.ip_loop))
            config.append ('!')
            for i in self.interfaces:
                config.append ('interface %s' % i.name)
                if (i.ip != None):
                    if (i.type == 'link'):
                        config.append (' ipv4 address %s/31' % (i.ip))
                    else:
                        config.append (' ipv4 address %s' % (i.ip))
                config.append (' no shutdown')
                config.append ('!')
            # Define ospf
            if (device.devcfg_defaults['ospf_enable'] == 1):
                config.append ('router ospf 100')
                config.append (' address-family ipv4 unicast')
                config.append (' area 0')
                if (device.devcfg_defaults['mpls_te_enable'] == 1):
                    config.append ('  mpls traffic-eng')
                config.append ('  interface loopback 0')
                config.append ('   passive enable')
                config.append ('  !')
                for i in self.interfaces:
                    if ( i.type == 'link'):
                        config.append ('  interface %s' % (i.name))
                        config.append ('  !')
                config.append (' !')
                if (device.devcfg_defaults['mpls_te_enable'] == 1):
                    config.append (' mpls traffic-eng router-id loopback0')
                config.append ('!')
            # Define isis
            if (device.devcfg_defaults['isis_enable'] == 1):
                if not 'isis_net' in self.options.keys():
                    self.options['isis_net'] = device.devcfg_defaults['isis_net']
                    device.devcfg_defaults['isis_net'] += 1
                x = self.options['isis_net']
                x_hex = '0'*(4-len(hex(x)[2:])) + str(hex(x)[2:])
                net_prefix = '49.0901.0000.0000.'
                net = net_prefix + x_hex + '.00'
                config.append ('router isis 100')
                config.append (' is-type level-2-only')
                config.append (' net '+ net) # need an incrementor here for the system ID 
                config.append (' address-family ipv4 unicast')
                if (device.devcfg_defaults['mpls_te_enable'] == 1):
                    config.append ('  metric-style wide')
                    config.append ('  mpls traffic-eng level-2-only')
                config.append (' !')
                config.append (' interface loopback 0')
                config.append ('  address-family ipv4 unicast')
                config.append ('  passive')
                config.append (' !')
                for i in self.interfaces:
                    if (i.type == 'link'):
                        config.append (' interface %s' % (i.name))
                        config.append ('  address-family ipv4 unicast')
                        config.append ('  !')
                        config.append (' !')
            if (device.devcfg_defaults['mpls_enable'] == 1):
                config.append ('!')
                config.append ('mpls ldp')
                config.append (' router-id Loopback 0')
                for i in self.interfaces:
                    if ( i.type == 'link'):
                        config.append ('  interface %s' % (i.name))
                        config.append ('  !')
            if (device.devcfg_defaults['mpls_te_enable'] == 1):
                config.append ('!')
                config.append ('rsvp')
                for i in self.interfaces:
                    if ( i.type == 'link'):
                        config.append ('  interface %s' % (i.name))
                        config.append ('   bandwidth 500 Mbps') 
                        config.append ('  !')
                config.append ('!')
                config.append ('mpls traffic-eng')
                for i in self.interfaces:
                    if ( i.type == 'link'):
                        config.append ('  interface %s' % (i.name))
                        config.append ('  !')
            config.append ('!')
            config.append ('end')
        if (config):
            if (fp == 1):
                return config
            else:
                print '%s snippet %s' % ('-' * 15, '-' * 15)
                for n, i in enumerate(config):
                    print i
                print '%s\n' % ('-' * 39)
        else: 
            print ('%s Configuration not available\n' % self.name)

    def showKVMConf(self,fp=None):
        kvmconfig = []
        cdrom = self.name + '.iso'
        if (self.model == 'XRVR'):
            kvmconfig.append('kvm -daemonize -nographic -m %s -hda %s.img -cdrom %s -serial telnet::%s,server,nowait' % (
                self.memory, self.name, cdrom, self.telnet_port ) )
        else:
            kvmconfig.append('kvm -daemonize -nographic -m %s -hda %s.img -serial telnet::%s,server,nowait' % (
                self.memory, self.name, self.telnet_port ) )
        for i in self.interfaces:
            kvmconfig.append('-net nic,model=%s,vlan=%s,macaddr=%s' % (
                i.model, i.vlan, i.mac ) )
 
        for i in self.interfaces:
            if i.connection:
                if i.connection.start_device.name == self.name:
                    contype = 'listen'
                else:
                    contype = 'connect'
                kvmconfig.append('-net socket,vlan=%s,%s=127.0.0.1:%s' % (
                    i.connection.vlan, contype, i.connection.transport ) )
        for n, i in enumerate(kvmconfig):
            if n == len(kvmconfig) - 1:
                delim = '\n'
            else:
                delim = '\\'
            if (fp == None): 
                print i, delim
            else:
                print >>fp, i, delim
                
            

class interface(object):
    """ Common base class for interface """
 
    def __init__(self,parent,name,vlan,model,mac,ip=None,type=None):
        self.parent = parent
        self.name = name
        self.model = model
        self.vlan = vlan
        self.mac = mac
        self.ip = ip
        self.type = type
        self.connection = None
 
    def __del__(self):
        pass
 
    def __str__(self):
        connects_to = ''
        if self.connection:
            start_dev = self.connection.start_device
            start_int = self.connection.start_int
            end_dev = self.connection.end_device
            end_int = self.connection.end_int
            if self.name == start_int.name and self.parent.name == start_int.parent.name:
                connects_to = 'link to: %s.%s telnet:%d' % (end_dev.name, end_int.name,
                                                        self.connection.transport) 
            else:
                connects_to = 'link to: %s.%s telnet:%d' % (start_dev.name, start_int.name,
                                                        self.connection.transport) 
        return "%-30s %-10s %-6d %-15s %-28s" % (self.parent.name + '.' + self.name,
            self.model, self.vlan, self.mac, connects_to)

    def createLink(self, end_int, vlan=None):
        if self.connection or end_interface.connection:
            # TODO: fix this message so that it shows which ports are in use
            print 'Error: ports are in use, cannot create a connection.'
            return None

        if not vlan:
            vlan = device.kvm_defaults['start_vlan']
            device.kvm_defaults['start_vlan'] += 1

        this_link = link(self.parent, end_interface.parent, vlan,
                         device.kvm_defaults['start_tcp'], self, end_int)
        device.kvm_defaults['start_tcp'] += 1
        self.connection = this_link
        end_int.connection = this_link
        return this_link
 
class link(object):
    """ Common base class for link. """
 
    def __init__(self, start_device, end_device, vlan, transport,
                 start_int=None, end_int=None):
        self.vlan = vlan             
        self.start_device = start_device
        self.start_int = start_int
        self.end_device = end_device
        self.end_int = end_int
        self.transport = transport
 
    def __del__(self):
        pass
 
    def __str__(self):
        return "%-25s %-25s %-10s %-10s" % (
            self.start_device.name+'.'+self.start_int.name,
            self.end_device.name+'.'+self.end_int.name, str(self.vlan), str(self.transport))
 
    def delete(self):
        self.start_int.connection = None
        self.end_int.connection = None

