#!/usr/bin/env python

################################################################################
# <copyright file="parseospf.py" owner="Tim Epkes and Paul Duda">
# Copyright (c) 2013 All Right Reserved, http://bitbucket.com/generatenetwork
#
# Please see the LICENSE.txt file for more information.
# All other rights reserved.
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
#
# </copyright>
# <author>Tim Epkes and Paul Duda</author>
# <email>tim.epkes@gmail.com and paul@zippity.net</email>
# <date>2013-01-20</date>
# <summary>Contains a base, abstract class for a parseospf</summary>
################################################################################


import sys
import re
from networkdevice import device

class parseospf(object):

    def __init__(self, input):
        self.input = input
        self.methods = {
            re.compile('advertising router: (?P<router>.+$)', re.I): self.getRouter,
            re.compile('number of links: (?P<numlinks>\d+)', re.I): self.getLinks,
        }
        self.properties = {'process': None, 'area': None, 'routers': [], 'router_count': -1, 'links': {} }
        for i, v in enumerate(self.input):
            self.input[i] = v.strip()
            for regex in self.methods.keys():
                the_match = re.search(regex, self.input[i])
                if the_match:
                    self.methods[regex](the_match, i)

    def createRouters(self):
        for d in self.properties['routers']:
            name = d['name']
            model = 'XRVR'
            if d['hasloop']:
                intcount = d['numlinks'] # normally would decrement here but XRVR has a defined mgmt int.
                loop = d['interfaces'][0]['id']
                print 'create %s router %s with %d interfaces' % ( model, name, intcount )
                device(name=name, ip_loop=loop, model=model, interfaces=intcount)
            else:
                intcount = d['numlinks']
                print 'create %s router %s with %d interfaces' % ( model, name, intcount )
                device(name=name, model=model, interfaces=intcount)

    def createLinks(self):
        self.links = self.properties['links']
        for l in self.links.keys():
            sd = device.getDevice(self.links[l]['sr'])
            dd = device.getDevice(self.links[l]['dr'])
            if not sd:
                print 'Error: could not find router %s.' % self.links[l]['sr']
                continue
            si = sd.getInterface(self.links[l]['si_num'] + 1) # hack: increment interface for XRVR device
        
            if not dd:
                print 'Error: could not find router %s.' % self.links[l]['dr']
                continue
            di = dd.getInterface(self.links[l]['di_num'] + 1)

            print 'create link %s.%s to %s.%s' % ( sd.name, si.name, dd.name, di.name )
            sd.createLink(dd, start_int=si.name, end_int=di.name)


    def getRouter(self, match, line):
        router_name = match.group('router').split('.')[0]
        self.properties['router_count'] += 1
        self.properties['routers'].append( {'name': router_name, 'numlinks': 0,
                                       'hasloop': False, 'interfaces': [] } )

    def getLinks(self, match, line):
        cdevice = self.properties['routers'][self.properties['router_count']]
        cdevice['numlinks'] = int(match.group('numlinks'))
        addr_re = re.compile('link (?P<atype>\w+).*[^:]+: (?P<addr>.+)$', re.I)
        link_re = re.compile('link connected to: a (?P<ltype>\w+) network', re.I)
        line += 2
        inum = 0
        links = self.properties['links']
        for i in range(0, int(cdevice['numlinks'])):
            link_match = re.search(link_re, self.input[line])
            interface = {'id': '', 'data': '', 'type': '', 'isloop': False}
            if link_match:
                interface['type'] = link_match.group('ltype').lower()
             
            for j in range(1, 3):
                line += 1
                addr_match = re.search(addr_re, self.input[line])
                if addr_match:
                    atype = addr_match.group('atype').lower()
                    addr = addr_match.group('addr')
                    interface[atype] = addr
            line += 4
            if interface['type'] == 'stub' and interface['data'] == '255.255.255.255':
                cdevice['hasloop'] = True
                interface['isloop'] = True
            if interface['type'] == 'transit':
                if not links.has_key(interface['id']):
                    links[interface['id']] = { 'sr': None, 'si_num': None, 'dr': None, 'di_num': None }
            
                if interface['id'] == interface['data']:
                    links[interface['id']]['dr'] = cdevice['name']
                    links[interface['id']]['di_num'] = inum
                else:
                    links[interface['id']]['sr'] = cdevice['name']
                    links[interface['id']]['si_num'] = inum
            cdevice['interfaces'].append( interface )
            # this won't work as expected if the loopback is not the first interface listed.
            if not interface['isloop']:
                inum += 1


if __name__ == '__main__':
    input = sys.stdin.readlines()
    obj = parseospf(input)
    obj.createRouters()
    obj.createLinks()
    device.showAllInterfaces()


