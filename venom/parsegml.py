#!/usr/bin/env python

################################################################################
# <copyright file="parsegml.py" owner="Tim Epkes and Paul Duda">
# Copyright (c) 2013 All Right Reserved, http://bitbucket.com/generatenetwork
#
# Please see the LICENSE.txt file for more information.
# All other rights reserved.
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
#
# </copyright>
# <author>Tim Epkes and Paul Duda</author>
# <email>tim.epkes@gmail.com and paul@zippity.net</email>
# <date>2013-01-20</date>
# <summary>Contains a base, abstract class for a parsegml</summary>
################################################################################


import sys
import re
from networkdevice import device

class parsegml(object):
    # This expects the GML attributes to be one per line
    def __init__(self, input):
        self.input = input
        self.methods = [
            { re.compile('^node$', re.I): self.doNode },
            { re.compile('^edge$', re.I): self.doEdge },
        ]
        self.properties = {'routers': [], 'router_count': -1 }
        for i, v in enumerate(self.input):
            self.input[i] = v.strip()
            for method in self.methods:
                if re.search(method.keys()[0], self.input[i]):
                    method.values()[0](i)
                    continue

    def doNode(self, line):
        self.properties['router_count'] += 1
        regexes = { 'id': re.compile('id\s+(?P<id>\d+)', re.I),
                'label': re.compile('label\s+"(?P<label>[\w\-\.]+)"', re.I) }
        router = { 'id': None, 'label': None, 'numlinks': 0, 'interfaces': [], 'loopbacks': 0 }
        while not router['id'] or not router['label']:
            line += 1
            for name, regex in sorted(regexes.iteritems()):
                the_match = re.search( regex, self.input[line] )
                if the_match:
                    router[name] = the_match.group(name)
                    continue
        self.properties['routers'].append(device(name=router['label'], model='XRVR', interfaces=1))
        #print 'id: %s, label: %s' % (router['id'], router['label'] )

    def doEdge(self, line):
        regexes = { 'source': re.compile('source\s+(?P<source>\d+)', re.I),
                'target': re.compile('target\s+(?P<target>\d+)', re.I) }
        edge = { 'source': None, 'target': None }
        while not edge['source'] or not edge['target']:
            line += 1
            for name, regex in sorted(regexes.iteritems()):
                the_match = re.search( regex, self.input[line] )
                if the_match:
                    edge[name] = the_match.group(name)
                    continue
        if int(edge['source']) > int(edge['target']):
            new_target = edge['source']
            edge['source'] = edge['target']
            edge['target'] = new_target
        
        routers = self.properties['routers']
        routers[int(edge['source'])].addInterface()
        routers[int(edge['target'])].addInterface()
        routers[int(edge['source'])].createLink(routers[int(edge['target'])])
        #print 'source: %s, target: %s' % (edge['source'], edge['target'] )

if __name__ == '__main__':
    input = sys.stdin.readlines()
    obj = parsegml(input)
    device.showAllInterfaces()
    device.showAllLinks()

