#!/usr/bin/env python

################################################################################
# <copyright file="parseisis.py" owner="Tim Epkes and Paul Duda">
# Copyright (c) 2013 All Right Reserved, http://bitbucket.com/generatenetwork
#
# Please see the LICENSE.txt file for more information.
# All other rights reserved.
#
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
#
# </copyright>
# <author>Tim Epkes and Paul Duda</author>
# <email>tim.epkes@gmail.com and paul@zippity.net</email>
# <date>2013-01-20</date>
# <summary>Contains a base, abstract class for a parseisis</summary>
################################################################################


import sys
import re
from networkdevice import device

class parseisis(object):

    def __init__(self, input):
        self.input = input
        self.methods = {
            re.compile('^(?P<router>[^\.]+)\.00-00', re.I): self.getRouter,
            re.compile('Metric:\s*(?P<metric>\d+)\s+IP-Extended (?P<ip>[^/]+)/(?P<bits>\d+$)', re.I): self.getInterface,
        }
        self.properties = {'routers': [], 'router_count': -1, 'links': {} }
        for i, v in enumerate(self.input):
            self.input[i] = v.strip()
            for regex in self.methods.keys():
                the_match = re.search(regex, self.input[i])
                if the_match:
                    self.methods[regex](the_match, i)
        self.getLinks()

    def createRouters(self):
        for d in self.properties['routers']:
            name = d['name']
            model = 'XRVR'
            if d['loopbacks'] > 0:
                # TODO: need to be able to handle more than one loopback in networkdevice
                # normally would decrement here but XRVR has a defined mgmt int.
                intcount = d['numlinks'] + d['loopbacks']
                loop = d['interfaces'][0]['ip']
                print 'create %s router %s with %d interfaces' % ( model, name, intcount )
                device(name=name, ip_loop=loop, model=model, interfaces=intcount)
            else:
                intcount = d['numlinks']
                print 'create %s router %s with %d interfaces' % ( model, name, intcount )
                device(name=name, model=model, interfaces=intcount)

    def createLinks(self):
        self.links = self.properties['links']
        for l in self.links.keys():
            sd = device.getDevice(self.links[l]['sr'])
            dd = device.getDevice(self.links[l]['dr'])
            if not sd:
                print 'Error: could not find router %s.' % self.links[l]['sr']
                continue
            si = sd.getInterface(self.links[l]['si_num'] + 1)
        
            if not dd:
                print 'Error: could not find router %s.' % self.links[l]['dr']
                continue
            di = dd.getInterface(self.links[l]['di_num'] + 1)

            print 'create link %s.%s to %s.%s' % ( sd.name, si.name, dd.name, di.name )
            sd.createLink(dd, start_int=si.name, end_int=di.name)


    def getRouter(self, match, line):
        router_name = match.group('router')
        self.properties['router_count'] += 1
        self.properties['routers'].append( {
            'name': router_name, 'numlinks': 0, 'hasloop': False, 'interfaces': [], 'loopbacks': 0 } )

    def getInterface(self, match, line):
        cdevice = self.properties['routers'][self.properties['router_count']]
        ip = match.group('ip')
        metric = match.group('metric')
        isloop = False
        if match.group('bits') == '32':
            cdevice['loopbacks'] += 1
            isloop = True
        else:
            cdevice['numlinks'] += 1
        cdevice['interfaces'].append({ 'ip': ip, 'metric': metric, 'isloop': isloop } )
        
    def getLinks(self):
        links = self.properties['links']
        for d in self.properties['routers']:
            inum = 0
            for n, i in enumerate(d['interfaces']):
                if i['metric'] == '0': continue # no link here this is a loop or passive
                if not links.has_key(i['ip']):
                    links[i['ip']] = { 'sr': d['name'], 'si_num': inum, 'dr': None, 'di_name': None }
                else:
                    links[i['ip']]['dr'] = d['name']
                    links[i['ip']]['di_num'] = inum 
                if not i['isloop']: inum += 1

if __name__ == '__main__':
    input = sys.stdin.readlines()
    obj = parseisis(input)
    obj.createRouters()
    obj.createLinks()
    device.showAllInterfaces()


