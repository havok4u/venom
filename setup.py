from distutils.core import setup

setup(
    name = 'venom',
    packages = ['venom'],
    version = '0.4.14',
    description = 'Generate a network of KVM devices.',
    author = 'Paul Duda, Tim Epkes',
    author_email = 'paul@zippity.net, tim.epkes@gmail.com',
    url = 'https://bitbucket.org/havok4u/venom/',
    license = 'BSD',
    classifiers = [
        'Programming Language :: Python', 
        'License :: OSI Approved :: BSD License',
        'Operating System :: Linux',
        'Development Status :: 5 - Alpha',
        'Environment :: Console',
    ],
    scripts = ['scripts/venom']
)
